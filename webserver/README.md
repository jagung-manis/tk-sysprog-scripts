# TK SysProg - Webserver
This django application will be run inside a virtual machine, to serve User Interfaces for managing network driver e1000 & ALSA soundcard.

## Author
* Evando Wihalim - 1806205445
* Jovi Handono Hutama - 1806205161
* Michael Susanto - 1806205653

## Techstack
* Django 3.1

## Run in local
* Clone this repository
```cmd
git clone https://gitlab.com/jagung-manis/tk-sysprog-scripts.git
```
* Create virtual environment
```cmd
python3 -m venv env
```
* Activate virtual environment (linux VM)
```cmd
source env/bin/activate
```
* Install dependencies
```cmd
cd tk-sysprog-scripts/webserver
pip3 install -r requirements.txt
```
* Migrate
```cmd
python3 manage.py makemigrations
python3 manage.py migrate
```
* Runserver and allow host to access VM webserver
```cmd
python3 manage.py runserver 0.0.0.0:8000
```

## Port Forwarding
* Open VirtualBox
* Choose your VM (ex: sysprog-ova)
* Go to Settings -> Network -> Adapter 1 -> Port Forwarding
* Click add Rule
* Insert:
    * Rule name: choose whatever you want
    * Protocol: TCP
    * Host IP: 127.0.0.1
    * Host Port: 1234 (or anything)
    * Guest IP: 10.0.2.15
    * Guest port: 8000
* Go to inside your Linux VM, run your server:
```cmd
python3 manage.py runserver 0.0.0.0:8000
```
* Go to your host machine's browser, type: localhost:1234

## Bootscript
* Copy **jagung-manis-web.service** to **/lib/systemd/system/**
```cmd
cp jagung-manis-web.service /lib/systemd/system/
```
* Copy **jagung-manis-init.sh** to **/etc/init.d/**
```cmd
cp jagung-manis-init.sh /etc/init.d/
```
* Change to executable (if it isn't in executable mode)
```cmd
chmod +x /etc/init.d/jagung-manis-init.sh
```
* Schedule this script to run every boot on crontab
```cmd
crontab -e
```
* Add this line
```cmd
@reboot /etc/init.d/jagung-manis-init.sh
```
* Enable script at startup
```cmd
systemctl daemon-reload
systemctl enable jagung-manis-web.service
```
* Note: use **sudo** if you can't run some commands.
