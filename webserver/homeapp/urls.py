from django.urls import path, include
from . import views

app_name = 'homeapp'
urlpatterns = [
    path('', views.home_view, name="home_url"),
    path('ethernet/', views.ethernet_view, name="ethernet_url"),
    path('sound/', views.sound_view, name="sound_url"),
    path('about/', views.about_view, name="about_url"),
]