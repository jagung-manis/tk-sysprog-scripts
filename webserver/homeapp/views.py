from django.shortcuts import render
from subprocess import *

# Create your views here.
def home_view(request):
    return render(request, "home.html")

def ethernet_view(request):
    if request.method == 'POST':
        dev_name = request.POST['device_name']
        speed = request.POST['speed']
        duplex = request.POST['duplex']
        process = run(['ethtool', '-s', dev_name, 'speed', speed, 'duplex', duplex], stdout=PIPE)
    script = '/home/user/tk-sysprog-scripts/webserver/homeapp/get_ethernet_devices.sh'
    get_dev_process = run(['bash', script], stdout=PIPE)
    device_info_raw = get_dev_process.stdout.decode('utf-8').split('\n')
    devices = []
    for device in device_info_raw:
        if(device != ''):
            device_info = device.split(';')
            print(device_info)
            dev_name = device_info[0]
            addr = device_info[1]
            driver = device_info[2]
            status = device_info[3]
            alm = device_info[4]
            devices.append({'dev_name':dev_name, 'addr':addr, 'driver':driver, 'status':status, 'alm':alm})
    return render(request, "ethernet.html", {'devices':devices})

def sound_view(request):
    if request.method == 'POST':
        if 'play_button' in request.POST:
            process1 = run(['aplay', '/usr/share/sounds/alsa/Front_Center.wav'], stdout=PIPE)

        if 'volume_level' in request.POST:
            try:
                volume = int(request.POST['volume_level'])
            except TypeError:
                volume = 0

            if volume > 0:
                process2 = run(['amixer', '-q', 'sset', 'Master', f'{volume}%+'])
            elif volume < 0:
                volume = abs(volume)
                process2 = run(['amixer', '-q', 'sset', 'Master', f'{volume}%-'])
    return render(request, "sound.html")

def about_view(request):
    return render(request, "about-us.html")
