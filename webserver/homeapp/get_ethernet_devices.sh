#!/bin/bash
function seeEthernetDD {
	for f in /sys/class/net/*; do
		dev=$(basename $f)
		driver=$(readlink $f/device/driver/module)
		if [ $driver ]; then
			driver=$(basename $driver)
		fi
		addr=$(cat $f/address)
		operstate=$(cat $f/operstate)
		adv_link_modes=$(ethtool $dev | grep "baseT" | tail -n +4 | sed -e 's/Advertised link modes://g' | awk '{$1=$1};1')
		IFS=$'\n' adv_link_modes_arr=($adv_link_modes)
		len=${#adv_link_modes_arr[@]}
		alm=$adv_link_modes
		if [[ $len -gt 1 ]]; then
			alm="1000baseT/Full"
		fi
		# alm=$(ethtool $dev | grep "baseT" | tail -n +4 | sed -e 's/Advertised link modes://g' | awk '{$1=$1};1')
		printf "%s;%s;%s;%s;%s" "$dev" "$addr" "$driver" "$operstate" "$alm"
		printf '\n'
	done
}

seeEthernetDD
