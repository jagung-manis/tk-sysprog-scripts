#!/bin/bash
DEV=""
flag=0

function printEqualSymbol {
	for (( i=0; i<$1; i++ ))
	do
		printf '='
	done
	printf '\n'
}

function printAsteriskSymbol {
	for (( i=0; i<$1; i++ ))
	do
		printf '*'
	done
	printf '\n'
}

function printDashSymbol {
	for (( i=0; i<$1; i++ ))
	do
		printf '-'
	done
	printf '\n'
}

function printSpace {
	for (( i=0; i<$1; i++ ))
	do
		printf ' '
	done
}

function printIntro {
	printEqualSymbol 52
	printSpace 20
	printf 'Jagung Manis'
	printSpace 20
	printf '\n'
	printEqualSymbol 52
	printAsteriskSymbol 52
	printf 'Authors:\n'
	printf '1. Evando Wihalim - 1806205445\n'
	printf '2. Jovi Handono Hutama - 1806205161\n'
	printf '3. Michael Susanto - 1806205653\n'
	printAsteriskSymbol 52
	printf 'Welcome!\nThis script is used to configure your ethernet device\n'
	printEqualSymbol 52
}

function pressEnter {
	printf '\n'
	read -p "Press [Enter] key to continue . . ."
	printf '\n'
}

function mainMenu {
	printEqualSymbol 29
	printSpace 10
	printf 'Main Menu'
	printSpace 10
	printf '\n'
	printEqualSymbol 29

	printf '1. See Network Devices & Drivers\n'
	printf '2. Change Speed and Duplex\n'
	printf '3. Check Ethernet Device Settings\n'
	printf '4. Play sample music\n'
	printf '5. Control audio volume\n'
	printf '6. Exit\n'
}

function menu1 {
	printEqualSymbol 52
	printSpace 10
	printf 'Showing Network Devices & Drivers'
	printSpace 10
	printf '\n'
	printEqualSymbol 52
}

function menu2 {
	printEqualSymbol 52
	printSpace 10
	printf 'Configure Network Devices & Drivers'
	printSpace 10
	printf '\n'
	printEqualSymbol 52
}

function menu3 {
	printEqualSymbol 52
	printSpace 10
	printf 'Showing Ethernet Device Settings'
	printSpace 10
	printf '\n'
	printEqualSymbol 52
}

function menu4 {
	printEqualSymbol 52
	printSpace 10
	printf 'Playing Sample Audio'
	printSpace 10
	printf '\n'
	printEqualSymbol 52
}

function menu5 {
	printEqualSymbol 52
	printSpace 10
	printf 'Setting up volume'
	printSpace 10
	printf '\n'
	printEqualSymbol 52
}

function seeEthernetDD {
	# taken from Worksheet 12 of System Programming course
	# Faculty of Computer Science
	# University of Indonesia
	printf "\n%10s %19s: %10s (%s)\n" "Device" "Address" "Driver" "Status"
	for f in /sys/class/net/*; do
		dev=$(basename $f)
		driver=$(readlink $f/device/driver/module)
		if [ $driver ]; then
			driver=$(basename $driver)
		fi
		addr=$(cat $f/address)
		operstate=$(cat $f/operstate)
		printf "%10s [%s]: %10s (%s)\n" "$dev" "$addr" "$driver" "$operstate"
	done
	pressEnter
}

function seeEthernetDD2 {
	printf "\n%10s %10s %10s\n" "Device" "Status" "Advertised link modes"
	for f in /sys/class/net/*; do
		dev=$(basename $f)
		operstate=$(cat $f/operstate)
		alm=$(ethtool $dev | grep "baseT" | tail -n +4 | sed -e 's/Advertised link modes://g')
		printf "%10s %10s %10s\n" "$dev" "$operstate" "$alm"
	done
	printf "\n"
}

function seeEthernetDDName {
	printf "\n%10s\n" "Device"
	for f in /sys/class/net/*; do
		dev=$(basename $f)
		printf "%10s\n" "$dev"
	done
	printf "\n"
}

function checkSuggestion {
	printf '\nSee ethernet devices & drivers menu to get more informations\n'
	printf "Type 'back' to return to main menu.\n"
}

function checkSuggestionAudio {
	printf '\nControl your audio\n'
	printf "Type 'back' to return to main menu.\n"
}

function getDevice {
	read -p "Enter device name: " DEV
}

function cancel {
	printf 'No configuration changed\n\n'
}

function changeSpeedDuplex {
	checkSuggestion
	seeEthernetDD2
	getDevice

	if [ "$DEV" = "back" ]; then
		cancel
		return
	fi

	setFlagDevice

	while [[ "$flag" -ne 1 ]]
	do
		echo "Please type a correct device name."
		getDevice
		if [ "$DEV" = "back" ]; then
			cancel
			return
		fi
		setFlagDevice
	done

	while [ 1 -gt 0 ]
	do
		read -p "Enter new speed [10/100/1000] Mbps: " speed

		if [ "$speed" = 10 ] || [ "$speed" = 100 ] || [ "$speed" = 1000 ]; then
			break
		elif [ "$speed" = "back" ]; then
			cancel
			return
		else
			printf '\nInvalid Input\n\n'
		fi
	done

	while [ 1 -gt 0 ]
	do
		read -p "Enter duplex [half/full]: " duplex

		if [ "$duplex" = "half" ] || [ "$duplex" = "full" ]; then
			break
		elif [ "$duplex" = "back" ]; then
			cancel
			return
		else
			printf '\nInvalid Input\n\n'
		fi
	done

	ethtool -s $DEV speed $speed duplex $duplex
	printf "Device %s speed advertised to %s with %s duplex mode\n\n" "$DEV" "$speed" "$duplex"	
	seeEthernetDD2
}

function changeAutoNeg {
	checkSuggestion
	seeEthernetDD2
	getDevice

	if [ "$DEV" = "back" ]; then
		cancel
		return
	fi
	
	setFlagDevice

	while [[ "$flag" -ne 1 ]]
	do
		echo "Please type a correct device name."
		getDevice
		back
		setFlagDevice
	done

	while [ 1 -gt 0 ]
	do
		read -p "Enter Auto Negotiation mode [on/off]: " autoneg

		if [ "$autoneg" = "on" ] || [ "$autoneg" = "off" ]; then
			break
		elif [ "$autoneg" = "back" ]; then
			cancel
			return
		else
			printf '\nInvalid Input\n\n'
		fi
	done
	
	speed=$(ethtool $DEV | grep "Advertised link modes: " | awk -F " " '{print $4}' | awk -F "baseT/" '{print $1}')
	duplex=$(ethtool $DEV | grep "Advertised link modes: " | awk -F " " '{print $4}' | awk -F "baseT/" '{print $2}')
	duplex=$(echo "$duplex" | tr '[:upper:]' '[:lower:]')
	ethtool -s $DEV speed $speed duplex $duplex autoneg $autoneg
	printf "Device %s auto-negotiation mode advertised to %s\n\n" "$DEV" "$autoneg" 
}

function checkEthernetSettings {
	echo
	echo "Type 'back' to return to main menu."
	seeEthernetDDName
	getDevice

	if [ "$DEV" = "back" ]; then
		cancel
		return
	fi

	setFlagDevice
	echo

	while [[ "$flag" -ne 1 ]]
	do
		echo "Please type a correct device name."
		getDevice

		if [ "$DEV" = "back" ]; then
			cancel
			return
		fi

		setFlagDevice
	done

	ethtool $DEV
	pressEnter
}

function setFlagDevice {
	all_device=$(ls /sys/class/net)
	flag=0

	for i in $all_device
	do
		if [[ "$DEV" == "$i" ]]; then
			flag=1
		fi
	done
}

function changeDebug {
	checkSuggestion
	seeEthernetDDName
	getDevice
	echo $DEV
	printf '4\n\n'
}

function playSampleMusic {
	aplay '/usr/share/sounds/alsa/Front_Center.wav'
	printf '\n'
}

function setVolume {
	checkSuggestionAudio

	while [ 1 -gt 0 ]
	do
		read -p "Do you want to increase / decrease volume? [i/d]: " id

		if [ "$id" = "i" ]; then
			echo "ok increase"
			id="+"
			break
		elif [ "$id" = "d" ]; then
			echo "ok decrease"
			id="-"
			break
		elif [ "$id" = "back" ]; then
			cancel
			return
		else
			echo "Invalid input"
		fi
	done

	while [ 1 -gt 0 ]
	do
		read -p "How many? [give non-negative number] type 'done' to save configuration: " volume

		if [ "$volume" -gt 0 ] 2>/dev/null; then
			amixer -q sset Master ${volume}%$id
			echo "ok volume set"
		elif [ "$volume" = "done" ];  then
			echo "Volume set, thank you."
			printf '\n'
			return
		else
			echo "Invalid number"
		fi
	done
}

function quit {
	echo "Thank you for using this script!"
	pressEnter
	exit
}

printIntro

while [ 1 -gt 0 ]
do
	echo $(date)
	mainMenu
	read -p "choose 1-6: " choice
	
	if [ "$choice" = 1 ]; then
		menu1
		seeEthernetDD
	elif [ "$choice" = 2 ]; then
		menu2
		changeSpeedDuplex
	elif [ "$choice" = 3 ]; then
		menu3
		checkEthernetSettings
	elif [ "$choice" = 4 ]; then
		menu4
		playSampleMusic
	elif [ "$choice" = 5 ]; then
		menu5
		setVolume
	elif [ "$choice" = 6 ]; then
		quit
	else
		printf "\nInvalid Input\n\n"
	fi
done
