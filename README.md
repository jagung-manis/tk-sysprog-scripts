# TK SysProg - CLI Script
This script (jagung_manis_cli.sh) will be run inside a virtual machine, to serve as User Interface for managing network driver e1000 and PCI soundcard driver. This script can change speed and duplex of an ethernet device with ethtool (tool to modify ethernet driver), and adjust audio system volume from amixer (tool to modify soundcard driver)

## Main File
* jagung-manis-cli.sh

## Author
* Evando Wihalim - 1806205445
* Jovi Handono Hutama - 1806205161
* Michael Susanto - 1806205653

## Techstack
* Ubuntu Server 18.04 sysprog-ova
* Linux Kernel 5.10.1

## Devices
* Ethernet (with e1000 driver)
* Soundcard (with PCI soundcard driver)
* Speaker (with snd_hda_intel driver)

## Dependencies (please install this first)
* ffplay (for playing .mp3 music)
```cmd
sudo apt install ffmpeg
```
* ALSA mixer (amixer) (Advanced Linux Sound Architecture) (configure soundcard driver)
```cmd
sudo apt install alsa-base
```

## Run in local
* Clone this repository
```cmd
git clone https://gitlab.com/jagung-manis/tk-sysprog-scripts.git
```
* Go inside folder
```cmd
cd tk-sysprog-scripts
```

## Apply Bootscript
* Copy **jagung-manis.service** to **/lib/systemd/system/**
```cmd
cp jagung-manis.service /lib/systemd/system/
```
* Copy **jagung-manis-cli.sh** to **/etc/init.d/**
```cmd
cp jagung-manis-cli.sh /etc/init.d/
```
* Change to executable (if it isn't in executable mode)
```cmd
chmod +x /etc/init.d/jagung-manis-cli.sh
```
* Schedule this script to run every boot on crontab
```cmd
crontab -e
```
* Add this line
```cmd
@reboot /etc/init.d/jagung-manis-cli.sh
```
* Enable script at startup
```cmd
systemctl daemon-reload
systemctl enable jagung-manis.service
```
* Note: use **sudo** if you can't run some commands.
